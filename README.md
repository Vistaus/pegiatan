# Pegiatan (/pɛˈɡi.ə.tan/)
[![Translation status](https://translate.codeberg.org/widgets/pegiatan/-/pegiatan/svg-badge.svg)](https://translate.codeberg.org/engage/pegiatan/)
[*Lihat README ini dalam bahasa Indonesia*](https://codeberg.org/Linerly/pegiatan/src/branch/main/README-id.md)

[![Translation status](https://translate.codeberg.org/widgets/pegiatan/-/pegiatan/svg-badge.svg)](https://translate.codeberg.org/engage/pegiatan/)
[*Lees deze informatie in het Nederlands*](https://codeberg.org/Linerly/pegiatan/src/branch/main/README-nl.md)

## What is Pegiatan?
Pegiatan is a simple web app for making your tasks more enjoyable. Similar to quest or mission systems in various games, you can turn your tasks into missions that you need to complete to earn rewards!

Currently a work in progress.

## Translation
Help translate the app on Codeberg Translate!

[![Translation status](https://translate.codeberg.org/widgets/pegiatan/-/pegiatan/multi-auto.svg)](https://translate.codeberg.org/engage/pegiatan/)