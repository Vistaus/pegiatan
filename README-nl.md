# Pegiatan (/pɛˈɡi.ə.tan/)
[![Translation status](https://translate.codeberg.org/widgets/pegiatan/-/pegiatan/svg-badge.svg)](https://translate.codeberg.org/engage/pegiatan/)

## Wat is Pegiatan?
Pegiatan is een eenvoudige webapp die het leuker maakt om je takenlĳst af te werken. De app is vergelĳkbaar met missies in games: je verdient beloningen door taken af te ronden!

Let op: de app is nog volop in ontwikkeling.

## Vertalen
Help met vertalen op Codeberg Translate!

[![Translation status](https://translate.codeberg.org/widgets/pegiatan/-/pegiatan/multi-auto.svg)](https://translate.c
